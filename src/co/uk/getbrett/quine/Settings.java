package co.uk.getbrett.quine;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jibble.pircbot.PircBot;

public class Settings extends PircBot
{

	private Map<String, String> settings = new HashMap<String, String>();
	
	public Settings()
	{
		// Basic settings
		settings.put("nickname", "quine-test");
		settings.put("server", "zulu.diverse.net.nz");
		settings.put("channel", "#._.");
		
		// Logging, parsing or other settings
		this.setAutoNickChange(true);
		this.setMessageDelay(0);
		Logger.getLogger("com.almworks.sqlite4java").setLevel(Level.OFF); // Turn off annoying SQL logging	
	}
	
	/**
	 * Return a map of the bot configuration
	 * @return this.config
	 */
	public Map<String, String> getConfig()
	{
		return settings;
	}
	
	/**
	 * Override the Pircbot logging to hide ping/pong events
	 */
	@Override
	public void log(String line) 
	{
		if (!line.contains("PING") && !line.contains("PONG")) 
			System.out.println(line);
    }
}