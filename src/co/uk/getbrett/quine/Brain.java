package co.uk.getbrett.quine;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import com.almworks.sqlite4java.SQLiteConnection;
import com.almworks.sqlite4java.SQLiteException;
import com.almworks.sqlite4java.SQLiteStatement;

public class Brain 
{
	
	private static SQLiteConnection db = new SQLiteConnection(new File("brain.db"));

	/**
	 * Takes an SQL query and runs it on the brain database
	 * @param query
	 * @return SQLiteStatement of matching rows
	 * @throws SQLiteException
	 */
	private static SQLiteStatement think(String query) throws SQLiteException
	{
		db.open(true);
		SQLiteStatement st = db.prepare(query);
		db.dispose();
		return st;
	}

}
