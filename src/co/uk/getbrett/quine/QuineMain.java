package co.uk.getbrett.quine;

import java.util.HashMap;
import java.util.Map;

public class QuineMain 
{	
	private static Map<String, String> config = new HashMap<String, String>();
	private static Quine bot = new Quine();
	
	/**
	 * @param Main class of Quine
	 */
	public static void main(String[] args) throws Exception
	{
		Settings settings = new Settings();
		config = settings.getConfig();
		
		// Startup events
		bot.setVerbose(true);
		bot.connect(config.get("server"));
		bot.joinChannel(config.get("channel"));
	}

}
